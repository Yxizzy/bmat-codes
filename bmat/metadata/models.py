from django.db import models

# Create your models here.
class Contributors(models.Model):
    id = models.AutoField(primary_key=True)
    contributors = models.CharField(max_length=30)

class Source(models.Model):
    id = models.AutoField(primary_key=True)
    source = models.CharField(max_length=30)

# intended design for production if splitting the tables
# class Data(models.Model):
#     id = models.AutoField(primary_key=True)
#     title = models.CharField(max_length=30)
#     contributors = models.ForeignKey(Contributors, on_delete=models.CASCADE)
#     iswc = models.CharField(max_length=20)
#     source = models.ForeignKey(Source, on_delete=models.CASCADE)

# current structure for the test purpose
class Data(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=30)
    contributors = models.CharField(max_length=500)
    iswc = models.CharField(max_length=20)
    source = models.CharField(max_length=300)


