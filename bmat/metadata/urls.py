from django.conf.urls import url, include
from rest_framework import routers
from .views import DataViewSet, SingleDataViewSet, ExportDataViewSet

#Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'data/(?P<iswc>[^/]+)', SingleDataViewSet, basename='single-data')
router.register(r'data', DataViewSet, basename='data')
router.register(r'export-data', ExportDataViewSet, basename='export-data')

urlpatterns = [
    url(r'^', include(router.urls)),
]