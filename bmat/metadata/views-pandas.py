from rest_framework import viewsets
from .models import Data
from .serializers import DataSerializer
import json
from rest_framework.response import Response
import pandas as pd
from collections import OrderedDict
import time

# ViewSets define the view behavior.
    
class DataViewSet(viewsets.ModelViewSet):
    queryset = Data.objects.all()
    serializer_class = DataSerializer
    
    def create(self, request):
        file = request.FILES['files']

        #check file type
        if not file.name.endswith('.csv'):
            return Response('File is not CSV type')

        #if file is too large, return
        if file.multiple_chunks():
            return Response("Uploaded file is too big (%.2f MB)." % (csv_file.size/(1000*1000),))
        
        start = time.time()
        df = pd.read_csv(file)

        title  =df.groupby(['iswc']).apply(lambda x: ' '.join(x.title)).reset_index()
        contributors  =df.groupby(['iswc']).apply(lambda x: ' '.join(x.contributors)).reset_index()
        source  =df.groupby(['iswc']).apply(lambda x: ' '.join(x.source)).reset_index()
        # id  =df.groupby(['iswc']).apply(lambda x: ' '.join(str(x.id))).reset_index()

        new_df = title.set_index('iswc').join(contributors.set_index('iswc'), lsuffix='_new').join(source.set_index('iswc'), lsuffix='_new')

        new_df['title'] = (new_df['0_new'].str.split()
                                    .apply(lambda x: OrderedDict.fromkeys(x).keys())
                                    .str.join(' '))

        new_df['contributors'] = (new_df['0'].str.split()
                                    .apply(lambda x: OrderedDict.fromkeys(x).keys())
                                    .str.join(' '))

        new_df['source'] = (new_df[0].str.split()
                                    .apply(lambda x: OrderedDict.fromkeys(x).keys())
                                    .str.join(' '))

        print(new_df.drop(columns=[0, '0_new', '0']))
        end = time.time()
        print(f"Runtime of the program is {end - start}")
        return Response(new_df)
        #0.04440498352050781