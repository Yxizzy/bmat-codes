from rest_framework import viewsets
from .models import Data
from .serializers import DataSerializer
import json
from rest_framework.response import Response
import itertools
from collections import Counter
import time
from sqlalchemy import create_engine
import pandas

# ViewSets define the view behavior.
def keyfunc(x):
    return x['iswc']

def unique_list(l):
    ulist = []
    [ulist.append(x) for x in l if x not in ulist]
    return ulist
    
class DataViewSet(viewsets.ModelViewSet):
    queryset = Data.objects.all()
    serializer_class = DataSerializer
    
    def create(self, request):
        file = request.FILES['files']
        
        #check file type
        if not file.name.endswith('.csv'):
            return Response('File is not CSV type')

        #if file is too large, return
        if file.multiple_chunks():
            return Response("Uploaded file is too big (%.2f MB)." % (csv_file.size/(1000*1000),))
        
        # start = time.time() 
        file_data = file.read().decode("utf-8")
        lines = file_data.split("\n")

        # check file format to ensure the required format/template was uploaded
        if lines[0] != 'title,contributors,iswc,source,id':
            return Response("Incorrect file format uploaded, please check and confirm you are using correct template")
        
        #loop over the lines and save them in db. If error , store as string and then display
        list_of_dict = []
        for line in lines:
            try:
                fields = line.split(",")
                data_dict = {}
                data_dict["title"] = fields[0]
                data_dict["contributors"] = fields[1]
                data_dict["iswc"] = fields[2]
                data_dict["source"] = fields[3]
                list_of_dict.append(data_dict)
            except Exception as e: print(e)
        del list_of_dict[0]
        
        for iswc, group in itertools.groupby(list_of_dict, keyfunc):
            if iswc == "":
                pass
            else:
                new_group = list(group)
                concat_value = {
                    k: [d.get(k) for d in new_group]
                    for k in set().union(*new_group)
                }

                # clean up title
                concat_value['title'] = ' '.join(concat_value['title'])
                concat_value['title'] = ' '.join(unique_list(concat_value['title'].split()))
                
                #clean up contributor
                concat_value['contributors'] = ' '.join(concat_value['contributors'])
                concat_value['contributors'] = ' '.join(unique_list(concat_value['contributors'].split()))

                #clean up sources
                concat_value['source'] = ' '.join(concat_value['source'])
                concat_value['source'] = ' '.join(unique_list(concat_value['source'].split()))

                #save reconciled data to the database
                # iswc is unique, check if iswc exists already and update fields else create a new record
                Data.objects.update_or_create(iswc=iswc, defaults={
                    'title' : concat_value['title'],
                    'contributors' : concat_value['contributors'],
                    'source' : concat_value['source']
                })

        # uncomment this section the check execution time
        # end = time.time()
        # print(f"Runtime of the program is {end - start}")


        return Response("Data Created")

class SingleDataViewSet(viewsets.ModelViewSet):
    
    """
    this function filters data by iswc and returns response in json
    """
    def list(self, request, iswc):
        queryset= Data.objects.filter(iswc=iswc).distinct()
        serializers = DataSerializer(queryset, many=True)
        return Response(serializers.data)

class ExportDataViewSet(viewsets.ModelViewSet):

    """
    This function generates csv from the models and returns the link to download file
    """
    def list(self, request):
        conn = create_engine('postgresql://postgres:x6mVkLYQZ9n7d7RY@database-1.cidfhcas5b70.us-west-1.rds.amazonaws.com:5432/metadata')
        query = str(Data.objects.all().query)

        #generate csv file using pandas
        results = pandas.read_sql_query(query, conn)
        results.to_csv("./metadata.csv", index=False)

        #optional, if file url is needed for download
       
        try:    
            with open('./metadata.csv', 'r') as f:
                file_data = f.read()

                # sending response 
                response = Response(file_data, content_type='application/csv')
                response['Content-Disposition'] = 'attachment; filename="metadata.csv"'
                return response
        except:
            return Response("csv generated")