import csv
import itertools
from collections import Counter
import time

"""
    This function is written to analyse the works_metadata.csv, get rid of duplicate values and 
"""
def keyfunc(x):
    return x['iswc']

def unique_list(l):
    ulist = []
    [ulist.append(x) for x in l if x not in ulist]
    return ulist
with open('works_metadata.csv') as file:
    # file_data = csv.reader(file)

    start = time.time()
    file_data = file.read()
    lines = file_data.split("\n")

    #loop over the lines and save them in db. If error , store as string and then display
    list_of_dict = []
    for line in lines:
        try:
            fields = line.split(",")
            data_dict = {}
            data_dict["title"] = fields[0]
            data_dict["contributors"] = fields[1]
            data_dict["iswc"] = fields[2]
            data_dict["source"] = fields[3]
            list_of_dict.append(data_dict)
        except Exception as e: print(e)
    # print(list_of_dict)
    for iswc, group in itertools.groupby(list_of_dict, keyfunc):
        new_group = list(group)
        concat_value = {
            k: [d.get(k) for d in new_group]
            for k in set().union(*new_group)
        }

        # clean up title
        concat_value['title'] = ' '.join(concat_value['title'])
        concat_value['title'] = ' '.join(unique_list(concat_value['title'].split()))
        
        #clean up contributor
        concat_value['contributors'] = ' '.join(concat_value['contributors'])
        concat_value['contributors'] = ' '.join(unique_list(concat_value['contributors'].split()))

        #clean up sources
        concat_value['source'] = ' '.join(concat_value['source'])
        concat_value['source'] = ' '.join(unique_list(concat_value['source'].split()))
        print(concat_value['source'])
        print(concat_value['title'])
        print(concat_value['contributors'])
        print(concat_value['iswc'])
    end = time.time()
    print(f"Runtime of the program is {end - start}")
    