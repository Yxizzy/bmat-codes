import pandas as pd
from collections import OrderedDict


df = pd.read_csv("works_metadata.csv")

title  =df.groupby(['iswc']).apply(lambda x: ' '.join(x.title)).reset_index()
contributors  =df.groupby(['iswc']).apply(lambda x: ' '.join(x.contributors)).reset_index()
source  =df.groupby(['iswc']).apply(lambda x: ' '.join(x.source)).reset_index()
# id  =df.groupby(['iswc']).apply(lambda x: ' '.join(str(x.id))).reset_index()

new_df = title.set_index('iswc').join(contributors.set_index('iswc'), lsuffix='_new').join(source.set_index('iswc'), lsuffix='_new')

new_df['title'] = (new_df['0_new'].str.split()
                              .apply(lambda x: OrderedDict.fromkeys(x).keys())
                              .str.join(' '))

new_df['contributors'] = (new_df['0'].str.split()
                              .apply(lambda x: OrderedDict.fromkeys(x).keys())
                              .str.join(' '))

new_df['source'] = (new_df[0].str.split()
                              .apply(lambda x: OrderedDict.fromkeys(x).keys())
                              .str.join(' '))

df_new = new_df.drop(columns=[0, '0_new', '0'])
print(df_new)